#! /bin/bash
# Create virtual enviroment if specified by user
set -e
cd /home/mpiuser/sfs/"${USERNAME}"
mkdir -p "${BASE_DIR}"
cd "${BASE_DIR}"
mkdir -p "${RUN_DIR}"
cd "${RUN_DIR}"
if [ ! "${USER_REQS}" == "None" ]
then
    # Creating virtual enviroment
    python3 -m venv --system-site-packages "${USERNAME}"_env
    # shellcheck disable=SC1090
    source "${USERNAME}"_env/bin/activate
    pip install cwlref-runner
    wget "${USER_REQS}"
    # Split string with delimeter "/" and get last split argument
    pip install -r "${USER_REQS##*/}"
    rm "${USER_REQS##*/}"
    deactivate
    # mpi requirements sharing
    ln -s /home/mpiuser/docker/dispel4py .
    cd dispel4py
    export PATH="/home/mpiuser/sfs/${USERNAME}/${BASE_DIR}/${RUN_DIR}/${USERNAME}_env/bin/"
    python -c "import sys; print(sys.executable)"
    python setup.py install
    source /etc/profile
    cd ..
    unlink dispel4py
fi
