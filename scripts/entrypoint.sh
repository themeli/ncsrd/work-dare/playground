#!/usr/bin/env bash
set -e
cd /home/mpiuser/sfs/"${USERNAME}"/"${BASE_DIR}"/"${RUN_DIR}"
find /home/mpiuser/docker/* -maxdepth 1 -type f | xargs cp -t .
# Create virtual enviroment if specified by user
OUTDIR="output"
mkdir -p ${OUTDIR}
if [ "${USER_REQS}" == "None" ]
then
    # Start execution
    while true; do
      bash start_simple.sh ${OUTDIR} && cd ${OUTDIR} && for i in `ls -d */`;do [ "$(ls -A $i)" ] && \
                  cp -p $i/* . && rm -rf $i || rm -rf $i;done
      break
    done
else
    # Start execution
    while true; do
          export PATH="/home/mpiuser/sfs/${USERNAME}/${BASE_DIR}/${RUN_DIR}/${USERNAME}_env/bin:/bin:/usr/bin:/usr/local/bin/cwl-runner" && \
          sed -i 's/\(secure_path="\)[^"]*"/\1\/home\/mpiuser\/sfs\/'"${USERNAME}"'\/'"${BASE_DIR}"'\/'"${RUN_DIR}"'\/'"${USERNAME}"'_env\/bin:\/bin:\/usr\/bin:\/usr\/local\/bin\/cwl-runner"/' /etc/sudoers && \
          sleep 7 && bash start_simple.sh ${OUTDIR} && source /etc/profile && rm -f reqs.txt && \
          rm -rf "${USERNAME}_env" && \
      cd ${OUTDIR} && for i in `ls -d */`;do [ "$(ls -A $i)" ] && \
                  cp -p $i/* . && rm -rf $i || rm -rf $i;done
      break
    done
fi

adduser nginx --disabled-password --gecos ""
groupadd appusers
usermod -a -G appusers nginx
usermod -a -G appusers mpiuser
chgrp -R appusers /home/mpiuser
chmod -R 777 /home/mpiuser

