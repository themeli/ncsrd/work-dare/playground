#! /bin/bash
chown mpiuser:mpiuser $1
export PATH="/home/mpiuser/sfs/${USERNAME}/${BASE_DIR}/${RUN_DIR}/${USERNAME}_env/bin:/bin:/usr/bin:/usr/local/bin"
cwl-runner --tmp-outdir-prefix=./$1/ --tmpdir-prefix=./$1/ d4p-simple.cwl spec.yaml
