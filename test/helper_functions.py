import json
from os import getcwd
from os.path import join, exists

import requests
import yaml


# ****************************************** Login *********************************************
def read_credentials():
    cred_file = join(getcwd(), "credentials.yaml")
    example_cred_file = join(getcwd(), "example_credentials.yaml")
    cred_filename = cred_file if exists(cred_file) else example_cred_file
    with open(cred_filename, "r") as f:
        return yaml.safe_load(f)


def login(hostname, username, password, requested_issuer):
    data = {
        "username": username,
        "password": password,
        "requested_issuer": requested_issuer
    }
    headers = {"Content-Type": "application/json"}
    r = requests.post(hostname + '/auth', data=json.dumps(data), headers=headers)
    if r.status_code == 200:
        response = json.loads(r.text)
        return {"access_token": response["access_token"], "refresh_token": response["refresh_token"]}
    else:
        print("Could not authenticate user!")


# ****************************************** Workspaces ************************************************
# Get workspace url by name
def get_workspace(name, hostname, token):
    # Get json response
    req = requests.get(hostname + '/workspaces/',
                       params={"access_token": token, "name": name})
    resp_json = json.loads(req.text)
    if type(resp_json) == dict:
        return resp_json["url"], resp_json["id"]
    elif type(resp_json) == list:
        # Iterate and retrieve
        return ([i['url'] for i in resp_json if i['name'] == name][0],
                [i['id'] for i in resp_json if i['name'] == name][0])


# Create workspace using d4p registry api
def create_workspace(clone, name, desc, hostname, token):
    # Prepare data for posting
    data = {
        "clone_of": clone,
        "name": name,
        "access_token": token,
        "description": desc
    }
    # Request for d4p registry api
    _r = requests.post(hostname + '/workspaces/', data=data)

    # Progress check
    if _r.status_code == 201:
        print('Added workspace: ' + name)
        return get_workspace(name, hostname, token)
    else:
        print('Add workspace resource returns status_code: ' +
              str(_r.status_code))
        return get_workspace(name, hostname, token)


def delete_workspace(name, hostname, token):
    workspace_url, wid = get_workspace(name, hostname, token)
    _r = requests.delete(hostname + '/workspaces/' + str(wid) + '/',
                         data={"access_token": token})
    # Progress check
    if _r.status_code == 204:
        print('Deleted workspace ' + name)
    else:
        print('Delete workspace returned status code: ' + str(_r.status_code))
        print(_r.text)


# ****************************************** PEs & PEImpls ************************************************
# Create ProcessingElement using d4p registry api
def create_pe(desc, name, conn, pckg, workspace, clone, peimpls, hostname, token):
    assert isinstance(conn, list)
    assert isinstance(peimpls, list)

    # Prepare data for posting
    data = {
        "description": desc,
        "name": name,
        "connections": conn,
        "pckg": pckg,
        "workspace": workspace,
        "clone_of": clone,
        "access_token": token,
        "peimpls": peimpls
    }
    # Request for d4p registry api
    _r = requests.post(hostname + '/pes/', data=data)
    # Progress check
    if _r.status_code == 201:
        print('Added Processing Element: ' + name)
        return json.loads(_r.text)['url']
    else:
        print('Add Processing Element resource returns status_code: ' +
              str(_r.status_code))


# Create ProcessingElement Implementation using d4p registry api
def create_peimpl(desc, code, parent_sig, pckg, name, workspace, clone, hostname, token):
    # Prepare data for posting
    data = {
        "description": desc,
        "access_token": token,
        "code": code,
        "parent_sig": parent_sig,
        "pckg": pckg,
        "name": name,
        "workspace": workspace,
        "clone_of": clone
    }
    # Request for d4p registry api, verify=False is only for demo purposes
    # nginx is too slow on response, open issue
    _r = requests.post(hostname + '/peimpls/', data=data, verify=False)
    # Progress check
    if _r.status_code == 201:
        print('Added Processing Element Implementation: ' + name)
        return json.loads(_r.text)['id']
    else:
        print('Add Processing Element Implementation resource returns \
                status_code: ' + str(_r.status_code))


# ****************************************** Playground ************************************************
def debug_d4p(hostname, impl_id, pckg, workspace_id, pe_name, token, reqs=None, output_filename="output.txt",
              **kw):
    # Prepare data for posting
    data = {
        "impl_id": impl_id,
        "pckg": pckg,
        "wrkspce_id": workspace_id,
        "n_nodes": 1,
        "name": pe_name,
        "access_token": token,
        "output_filename": output_filename,
        "reqs": reqs if not (reqs is None) else "None"
    }
    d4p_args = {}
    for k in kw:
        d4p_args[k] = kw.get(k)
    data['d4p_args'] = json.dumps(d4p_args)
    r = requests.post(hostname + '/playground', data=data)
    if r.status_code == 200:
        response = json.loads(r.text)
        if response["logs"]:
            print("Logs:\n========================")
            for log in response["logs"]:
                print(log)
        if response["output"]:
            print("Output content:\n==============================")
            for output in response["output"]:
                print(output)
    else:
        print('Playground returns status_code: \
                ' + str(r.status_code))
        print(r.text)


def exec_command(hostname, token, command, run_dir="new", output_filename="output.txt"):
    data = {
        "access_token": token,
        "command": command,
        "run_dir": run_dir,
        "output_filename": output_filename
    }

    r = requests.post(hostname + '/run-command', data=data)
    if r.status_code == 200:
        response = json.loads(r.text)
        if response["logs"]:
            print("Logs:\n========================")
            for log in response["logs"]:
                print(log)
        if response["output"]:
            print("Output content:\n==============================")
            for output in response["output"]:
                print(output)
        if response["run_dir"]:
            print("Run directory is: ")
            print(response["run_dir"])
    else:
        print('Playground returns status_code: \
                ' + str(r.status_code))
        print(r.text)


def upload(token, path, local_path, filename, hostname, dataset_name="N/A"):
    data = {
        "dataset_name": dataset_name,
        "access_token": token,
        "path": path,
        "filename": filename,
    }
    with open(join(local_path, filename), 'rb') as s:
        script = s.read()
    data["file"] = script
    _r = requests.post(hostname + '/upload', data=data)
    return _r.text


def download(hostname, token, run_dir, filename, local_path):
    url = hostname + "/download"

    data = {
        "access_token": token,
        "run_dir": run_dir,
        "filename": filename
    }
    response = requests.post(url, data=data)
    if response.status_code == 200:
        with open(join(local_path, filename), "wb") as f:
            f.write(response.content)
        print("Download complete!")
    else:
        print("An error occurred: {}".format(response.text))


def list_playground_dirs(token, hostname):
    url = hostname + "/list-runs"
    data = {
        "access_token": token
    }
    response = requests.post(url, data=data)
    if response.status_code == 200:
        dirs = json.loads(response.text)
        print("Files generated from trial runs.......")
        print('\n')
        for i in dirs["paths"]:
            print("Playground directory: {}".format(i))
            print('\n')
    else:
        print("An error occurred: {}".format(response.text))


def list_output_files(hostname, token, run_dir):
    url = hostname + "/output-files"
    data = {
        "access_token": token,
        "run_dir": run_dir
    }

    response = requests.post(url, data=data)
    if response.status_code == 200:
        files = json.loads(response.text)
        print('Listing files......')
        print('\n')
        for i in files["files"]:
            print("Output file: {}".format(i))
            print('\n')
        print('\n')
