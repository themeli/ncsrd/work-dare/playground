# Playground

The purpose of this component is to provide a DARE environment for test and debugging purposes. The component exposes two endpoints:

* The /playground endpoint: this simulates the dispel4py execution in DARE and prints the logs and output content directly to the user
* The /run-command endpoint: accepts any bash command, which is executed and returns the result to the user

## Use in notebook

* In the same directory with the helper\_functions script create a yaml file named credentials.yaml
    - Inside the yaml file store your credentials used to get authenticated to the platform. e.g.
    
  ```yaml
    username: user1
    password: 1234  
  ```
* For the first endpoint, you need to execute the first steps as always: login, create workspace, register the workflow
* For the second endpoint, you need to provide the endpoint, the token, the command and the output file name if exists

## Update helper_functions

Add the below two methods in helper_functions:

* For the first endpoint

```python 
def debug_d4p(hostname, impl_id, pckg, workspace_id, pe_name, token, reqs=None, output_filename="output.txt",
              **kw):
    # Prepare data for posting
    data = {
        "impl_id": impl_id,
        "pckg": pckg,
        "wrkspce_id": workspace_id,
        "n_nodes": 1,
        "name": pe_name,
        "access_token": token,
        "output_filename": output_filename,
        "reqs": reqs if not (reqs is None) else "None"
    }
    d4p_args = {}
    for k in kw:
        d4p_args[k] = kw.get(k)
    data['d4p_args'] = d4p_args
    r = requests.post(hostname + '/playground', data=json.dumps(data))
    if r.status_code == 200:
        response = json.loads(r.text)
        if response["logs"]:
            print("Logs:\n========================")
            for log in response["logs"]:
                print(log)
        if response["output"]:
            print("Output content:\n==============================")
            for output in response["output"]:
                print(output)
    else:
        print('Playground returns status_code: \
                ' + str(r.status_code))
        print(r.text)
```
* For the second endpoint:
```python
import requests
import json

def exec_command(hostname, token, command, run_dir="new", output_filename="output.txt"):
    data = {
        "access_token": token,
        "command": command,
        "run_dir": run_dir,
        "output_filename": output_filename
    }

    r = requests.post(hostname + '/run-command', data=json.dumps(data))
    if r.status_code == 200:
        response = json.loads(r.text)
        if response["logs"]:
            print("Logs:\n========================")
            for log in response["logs"]:
                print(log)
        if response["output"]:
            print("Output content:\n==============================")
            for output in response["output"]:
                print(output)
        if response["run_dir"]:
            print("Run directory is: ")
            print(response["run_dir"])
    else:
        print('Playground returns status_code: \
                ' + str(r.status_code))
        print(r.text)

```

## Update the jupyter notebook

* For the /playground endpoint:

```python
F.debug_d4p(hostname=hostname, impl_id=impl_id, pckg="mysplitmerge_pckg", workspace_id=workspace_id, 
pe_name="mySplitMerge", token=F.auth(), creds=creds, no_processes=6, iterations=1,
            reqs='https://gitlab.com/project-dare/dare-api/raw/master/examples/jupyter/requirements.txt')
```

* For the /run-command endpoint:

```python
F.exec_command(PLAYGROUND_API_HOSTNAME, F.auth(), "pip install --user numpy")
```