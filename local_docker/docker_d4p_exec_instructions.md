# Instructions to execute locally

* Get the container code
* Update env_prep.sh & entrypoint.sh => USER_REQS: add the url in GitLab with the requirements.txt file
* Add your python script with the workflow inside the folder with the Dockerfile and name the file "code.py" (without provenance)
* Build the docker image
* Execute: 
	- docker images to get the image id you built
	- docker run -dit <docker image>
	- docker ps to get the container id
	- docker exec -it <container id> bash
* Inside the docker, just execute:
	- bash env_prep.sh
	- entrypoint.sh