#!/usr/bin/env bash
set -e
RUN_DIR="run"

cd /home/mpiuser/sfs/d4p/${RUN_DIR}
find /home/mpiuser/docker/* -maxdepth 1 -type f | xargs cp -t .
# Create virtual enviroment if specified by user
USER_REQS="requirements.txt"
USERNAME="Th1s4sY0urT0k3Nn"
OUTDIR="output"
mkdir -p ${OUTDIR}
if [ ${USER_REQS} == "None" ]
then
    # Start execution
    while true; do
        python d4p-jobstart.py && sleep 7 && if [ $(grep 'target:' spec.yaml | cut -c 9-) = "simple" ];then bash start_simple.sh ${OUTDIR}; else bash start_mpi.sh ${OUTDIR} ;fi && \
        cd ${OUTDIR} && for i in `ls -d */`;do [ "$(ls -A $i)" ] && \
                    cp -p $i/* . && rm -rf $i || rm -rf $i;done
        break
    done
else
    # Start execution
    while true; do
      export PATH="/home/mpiuser/sfs/d4p/${RUN_DIR}/${USERNAME}_env/bin:/bin:/usr/bin:/usr/local/bin/cwl-runner" && \
      sed -i 's/\(secure_path="\)[^"]*"/\1\/home\/mpiuser\/sfs\/d4p\/'"${RUN_DIR}"'\/'"${USERNAME}"'_env\/bin:\/bin:\/usr\/bin:\/usr\/local\/bin\/cwl-runner"/' /etc/sudoers && \
      sleep 7 && if [ $(grep 'target:' spec.yaml | cut -c 9-) = "simple" ];then bash start_simple.sh ${OUTDIR} ${PATH}; else bash start_mpi.sh ${OUTDIR} ${PATH};fi && \
      source /etc/profile && rm -f reqs.txt && \
      rm -rf "${USERNAME}_env" && \
  		cd ${OUTDIR} && for i in `ls -d */`;do [ "$(ls -A $i)" ] && \
              cp -p $i/* . && rm -rf $i || rm -rf $i;done
          break
    done
fi