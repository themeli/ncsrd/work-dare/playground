#! /bin/bash
chown mpiuser:mpiuser $1
export PATH="$2"
cwl-runner --tmp-outdir-prefix=./$1/ --tmpdir-prefix=./$1/ d4p-simple.cwl spec.yaml
