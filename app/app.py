import json
import os
import platform
import pwd
import uuid
from datetime import datetime
from os import mkdir, getlogin
from os.path import join, exists
from logging.config import dictConfig
import requests
import yaml
from flask import Flask, request, send_file, g
from kubernetes.config.config_exception import ConfigException
from werkzeug.utils import secure_filename

import utils


def get_d4p_specfem_sfs():
    """
    Reads the configuration yaml file of the playground module to retrieve information on the dispel4py and specfem
    mount paths.

    Returns
        dict: the properties dictionary to be used in the API endpoints. Contains the home and base directories
        where execution folders will be stored.
    """
    try:
        try:
            r = requests.get('https://gitlab.com/project-dare/dare-platform/-/raw/master/k8s/playground-dp.yaml')
            name_space = yaml.safe_load(r.text)['metadata']['namespace']
            # Current pod running flask api
            host_pod = utils.init_from_yaml(name_space)
            # Namespace found in yaml, but not in kubernetes
            if host_pod is None:
                name_space = 'default'
                # Current pod running flask api
                host_pod = utils.init_from_yaml(name_space)
        # No namespace in yaml
        except (ConfigException, Exception) as e:
            print(e)
            name_space = 'default'
            # Current pod running flask api
            host_pod = utils.init_from_yaml(name_space)

        # D4P OPENMPI
        _d4p = {'jobname': 'd4p-openmpi', 'mountname': host_pod.spec.containers[0].volume_mounts[0].name,
                'mountpath': host_pod.spec.containers[0].volume_mounts[0].mount_path,
                'volname': host_pod.spec.volumes[0].name,
                'fsname': host_pod.spec.volumes[0].flex_volume.options['fsName']}
        _code = _d4p['mountpath'] + host_pod.spec.volumes[0].flex_volume.options['path'].split('/')[1] + '/code.py'
        # SPECFEM
        _specfem = {'specname': 'specfem-openmpi', 'specvolname': host_pod.spec.containers[0].volume_mounts[1].name,
                    'spec_mountpath': host_pod.spec.containers[0].volume_mounts[1].mount_path,
                    'fsname': host_pod.spec.volumes[0].flex_volume.options['fsName']}
    except (ConfigException, Exception):
        operating_system_name = platform.system()
        path = ""
        if operating_system_name.lower() == "linux":
            try:
                if not exists(join("/home", getlogin(), "mpiuser")):
                    mkdir(join("/home", getlogin(), "mpiuser"))
                    mkdir(join("/home", getlogin(), "mpiuser", "sfs"))
                path = join("/home", getlogin(), "mpiuser", "sfs")
            except (OSError, Exception):
                if not exists("/home/mpiuser"):
                    mkdir("/home/mpiuser")
                mkdir("/home/mpiuser/sfs")
                path = "/home/mpiuser/sfs"
        elif operating_system_name.lower() == "windows":
            if not exists(join("C:\\Users", getlogin(), "mpiuser")):
                mkdir(join("C:\\Users", getlogin(), "mpiuser"))
                mkdir(join("C:\\Users", getlogin(), "mpiuser", "sfs"))
            path = join("C:\\Users", getlogin(), "mpiuser", "sfs")
        _d4p = {"mountpath": path}
    return {"base_directory": "debug", "home_dir": _d4p['mountpath']}


dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

app = Flask(__name__)
logger = app.logger
properties = get_d4p_specfem_sfs()


@app.route('/playground', methods=['POST'])
def playground():
    """
    Dispel4py execution simulation. The user should provide the name of the output file of the execution, the name
    of the PE implemenation, its id, the workspace id etc in order to execute an already registred code in the
    platform

    Returns
        dict: the logs and the contents of the output file
    """
    request_data = request.form
    logger.debug = "Got data from playground request {}".format(request_data)
    username = g.username

    home_dir = properties["home_dir"]
    base_dir = properties["base_directory"]

    # delegation_token_response = utils.issue_delegation_token(request_data, g)

    # if delegation_token_response[0] != 200:
    #     return "Could not issue a delegation token", 500

    # delegation_token_response = json.loads(delegation_token_response[1])
    # delegation_token = delegation_token_response["access_token"]
    # expires_in = delegation_token_response["expires_in"]
    delegation_token = request_data["access_token"]

    # get run data
    run_id = uuid.uuid4().hex
    run_dir = username + "_" + datetime.now().strftime('%Y%m%d%H%M%S') + "_" + str(run_id)

    # get output data
    output_filename = request_data["output_filename"]
    output_file_format = request_data["output_file_format"]
    output_dir = join(home_dir, username, base_dir, run_dir, utils.OUTPUT)
    output_file = join(output_dir, output_filename)

    # update environ and get D4P_DATA
    data = utils.define_environ(properties, request_data, run_dir, run_id, delegation_token, g)

    # get PEImpl
    specs = utils.get_code_and_specs(json.dumps(data), base_dir, run_dir, g)

    # create run and ouput folder
    utils.create_necessary_folders(properties, run_dir, username)
    # save code.py and spec.yaml files
    utils.save_code_and_spec(properties, run_dir, specs, username)
    # execute workflow
    command = " cd /home/mpiuser/docker && bash env_prep.sh && sleep 1 && bash entrypoint.sh"
    logs = utils.run_subprocess(command, output_dir)
    return json.dumps({"logs": logs, "output": utils.get_workflow_output(output_file, output_file_format)})


@app.route('/run-command', methods=['POST'])
def run_command():
    """
    Terminal simulation. The user can execute any command or script (if uploaded in the platform) and immediately
    get the output of the command. The user needs to provide the token, the command to be executed and the name
    of the directory where results will be stored. If users wish to create a new directory than they need to provide
    the word new for directory name.

    Returns
        dict: the logs and the output of the execution as well as the name of the directory that is used in case the
        users need to reuse it.
    """
    home_dir = properties["home_dir"]
    base_dir = properties["base_directory"]

    # get request data
    request_data = request.form

    username = g.username
    output_filename = request_data["output_filename"]
    command = request_data["command"]
    run_dir = request_data["run_dir"]

    # get run data
    if run_dir == "new":
        run_id = uuid.uuid4().hex
        run_dir = username + "_" + datetime.now().strftime('%Y%m%d%H%M%S') + "_" + str(run_id)

    # get output data
    output_dir = join(home_dir, username, base_dir, run_dir, utils.OUTPUT)
    output_file = join(output_dir, output_filename)

    # create run and ouput folder
    utils.create_necessary_folders(properties, run_dir, username)

    # change to run directory
    os.chdir(join(home_dir, username, base_dir, run_dir))

    # execute the given command
    logs = utils.run_subprocess(command, output_dir)
    return json.dumps({"logs": logs, "output": utils.get_workflow_output(output_file), "run_dir": run_dir})


@app.route("/upload", methods=['POST'])
def upload():
    """
    API endpoint providing the functionality to upload files in the DARE platform.
    All files are saved under the user's directory: ../username/uploads/. The users can provide a folder and a file
    name. If they provide a folder name the file will be stored under ../username/uploads/<folder name>/<file name>,
    otherwise the file will be saved directly in the uploads folder of the user.
    """
    # args
    data = request.form
    dataset_name = data["dataset_name"]
    path = data["path"]
    username = g.username
    user_uploads_folder = join(properties["home_dir"], username, utils.UPLOADS)
    # Check for missing arguments
    if not (None in [dataset_name]):
        file = data["file"]
        filename = data["filename"]
        # Upload
        filename = secure_filename(filename)
        # Check if out-dir exists
        try:
            file_folder = join(user_uploads_folder, path) if path else user_uploads_folder
            file_path = join(file_folder, filename)
            with open(file_path, "w") as f:
                f.write(file)
        # Create path and upload
        except (OSError, FileNotFoundError, Exception):
            if path:
                if not exists(join(user_uploads_folder, path)):
                    os.mkdir(join(user_uploads_folder, path))
                file_path = join(user_uploads_folder, path, filename)
                with open(file_path, "w") as f:
                    f.write(file)
        return 'OK!', 200
    else:
        return 'Missing arguments', 500


@app.route('/download', methods=["POST"])
def download_file():
    """
    Users provide the full path to a specific file they wish to download locally from the platform.

    Returns
        the file to be downloaded
    """
    home_dir = properties["home_dir"]
    base_dir = properties["base_directory"]

    request_data = request.form
    username = g.username

    run_dir = request_data["run_dir"]
    filename = request_data["filename"]

    path = join(home_dir, username, base_dir, run_dir, utils.OUTPUT, filename)
    try:
        return send_file(path, attachment_filename=filename)
    except (FileNotFoundError, OSError):
        return 'File not found', 404


@app.route('/list-runs', methods=['POST'])
def list_runs():
    """
    List of all the testing execution directories. Users need to provide their token in order to be authenticated.

    Returns
        str: json with the paths of the testing directories of the user
    """
    home_dir = properties["home_dir"]
    base_dir = properties["base_directory"]

    # use keycloak to authenticate the user - check if token is valid and get user data
    username = g.username
    run_dirs_path = join(home_dir, username, base_dir)
    run_dirs = os.listdir(run_dirs_path)
    paths = []
    if run_dirs:
        for run_dir in run_dirs:
            paths.append(run_dir)
    return json.dumps({"paths": paths})


@app.route('/output-files', methods=['POST'])
def list_output_files():
    """
    List of the output files of a specific execution. Users need to provide their token in order to be authenticated
    as well as the name of the execution directory.

    Returns
        str: json with the name of the output files (logs file and output files)
    """
    home_dir = properties["home_dir"]
    base_dir = properties["base_directory"]

    # get request data
    request_data = request.form
    username = g.username

    run_dir = request_data["run_dir"]
    output_files_path = join(home_dir, username, base_dir, run_dir, utils.OUTPUT)
    output_files = os.listdir(output_files_path)
    files = []
    if output_files:
        for output_file in output_files:
            files.append(output_file)
    return json.dumps({"files": files})


@app.before_request
def is_authenticated():
    request_data = request.form
    token = request_data["access_token"]
    try:
        host = os.environ[
            "DARE_LOGIN_PUBLIC_SERVICE_HOST"] if "DARE_LOGIN_PUBLIC_SERVICE_HOST" in os.environ else "localhost"
        port = os.environ[
            "DARE_LOGIN_PUBLIC_SERVICE_PORT"] if "DARE_LOGIN_PUBLIC_SERVICE_PORT" in os.environ else 80
        url = "http://{}:{}/validate-token".format(host, port)
        response = requests.post(url, data=json.dumps({"access_token": token}))
        if response.status_code == 200:
            result = json.loads(response.text)
            g.username = result["username"]
            g.user_id = result["user_id"]
            g.issuer = result["issuer"]
        else:
            msg = "Token is expired or user has not signed in! {}".format(response.text)
            return msg, 500
    except (ConnectionError, Exception) as e:
        print(e)
        msg = "Something went wrong! {}".format(e)
        return msg, 500


@app.route('/whoami', methods=['GET'])
def whoami():
    print(pwd.getpwuid(os.getuid())[0])
    print(os.path.expanduser("~"))


@app.route('/test-delegation-token', methods=['POST'])
def test_delegation_token():
    request_data = request.form
    delegation_token_response = utils.issue_delegation_token(request_data, g)

    if delegation_token_response[0] != 200:
        return "Could not issue a delegation token! {}".format(delegation_token_response[1]), 500

    delegation_token_response = json.loads(delegation_token_response[1])
    delegation_token = delegation_token_response["access_token"]
    expires_in = delegation_token_response["expires_in"]
    return json.dumps({"access_token": delegation_token, "expires_in": expires_in}), 200
